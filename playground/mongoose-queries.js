const { mongoose } = require('./../server/db/mongoose');
const { Todo } = require('./../server/models/todo');

const id = '59d51219998a9715b4ed31c4';

Todo.find({
    _id:id
}).then(todos => console.log(todos));

Todo.findOne({
    _id:id
}).then(todos => console.log(todos));

Todo.findById(id).then(e => console.log(e));
