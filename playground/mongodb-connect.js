// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Connected to MongoDB Server");

    // db.collection('Todos').insertOne({
    //     text: 'Happy to todo :D',
    //     completed: false
    // }, (err, result) => {
    //     if (err)
    //         return console.log("unable to insert", err);
    //     console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
    // });



    db.close();
});