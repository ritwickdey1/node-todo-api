const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Connected to MongoDB Server");

    db.collection('Todos')
        .find({
            completed: !true
        })
        .toArray()
        .then(docs => {
            console.log('Todos');
            console.log(JSON.stringify(docs, undefined, 2));
        })
        .catch(console.log);

    db.close();
});