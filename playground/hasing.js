const { SHA256 } = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

// var message = 'I am Ritwick Dey1';
// var hash = SHA256(message).toString();
// console.log(message, hash)

// let data = {
//     id: 10
// }
// let token = jwt.sign(data, 'mySecret')
// console.log(token)

let pass = 'abc123!';
bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(pass, salt, (err, hash) => {
        console.log(pass);
        console.log(salt);
        console.log(hash);
    });
});

const hased = '$2a$10$12.kLHcGvlvIc4sVwlhw0.VKEDIqeH4eo8RMrVQ1z5FYx9bgKPsSW';

bcrypt.compare(pass, hased, (err, res) => {
    console.log("Pass: ", res)
});