const mongoose = require('mongoose');
const { isEmail } = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: (v) => isEmail(v),
            message: '{VALUE} is not a valid Email!'
        },
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]

});

UserSchema.methods.generateAuthToken = function () {
    const user = this;
    const access = 'auth';
    const token = jwt.sign({
        _id: user._id.toHexString(),
        access: access
    }, process.env.JWT_SECRET);

    user.tokens.push({ access, token });

    return user.save().then(() => {
        return token;
    });
};

UserSchema.statics.findByToken = function (token) {
    const User = this;
    let decoded;

    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (error) {
        return Promise.reject();
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth',
    });
}

UserSchema.statics.findByCredencials = function (email, password) {

    return User.findOne({ email })
        .then(user => {
            if (!user) return Promise.reject();
            return new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password, (err, isValid) => {
                    return (!err && isValid) ? resolve(user) : reject();
                });
            });
        });

}

UserSchema.methods.toJSON = function () {
    const user = this;
    const userObject = user.toObject();
    return _.pick(userObject, ['_id', 'email']);
};

UserSchema.methods.removeToken = function (token) {
    const user = this;

    return user.update({
        $pull: {
            tokens: {
                token: token
            }
        }
    });
}

UserSchema.pre('save', function (next) {
    const user = this;
    if (user.isModified('password')) {
        const pass = user.password;
        bcrypt.genSalt(1, (err, salt) => { // salt 10 is toooo slow :/
            bcrypt.hash(pass, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    }
    else {
        next();
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = { User }