require('./config/config');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');

const { mongoose } = require('./db/mongoose');
const { Todo } = require('./models/todo');
const { User } = require('./models/user');
const { authenticate } = require('./middleware/authenticate');

const app = express();
const port = process.env.port;

app.use(bodyParser.json());

// POST /todos/
app.post('/todos', authenticate, (req, res) => {

    let todo = new Todo({
        text: req.body.text,
        _creator: req.user._id
    });

    todo.save()
        .then(doc => res.status(201).send(doc))
        .catch(err => res.status(400).send(err))

});

// GET /todos/
app.get('/todos', authenticate, (req, res) => {
    Todo.find({ _creator: req.user._id })
        .then(todos => res.send({ todos }))
        .catch(err => res.status(400).send(err));
});

// GET /todos/:id
app.get('/todos/:id', authenticate, (req, res) => {

    const id = req.params.id;

    if (!ObjectID.isValid(id))
        return res.status(404).send('not found');

    Todo.findOne({ _id: id, _creator: req.user._id })
        .then(todo => {
            if (!todo)
                return res.status(404).send('not found');
            res.send({ todo })
        })
        .catch(err => res.status(400).send(err));
});

// DELETE /todos/:id
app.delete('/todos/:id', authenticate, (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id))
        return res.status(404).send('not found');

    Todo.findOneAndRemove({ _id: id, _creator: req.user._id })
        .then(todo => {
            if (!todo)
                return res.status(404).send('not found');
            res.send({ todo });
        })
        .catch(err => res.status(400).send(err));

});

// PATCH /todos/:id
app.patch('/todos/:id', authenticate, (req, res) => {
    const id = req.params.id;

    let body = _.pick(req.body, ['text', 'completed']);

    if (!ObjectID.isValid(id))
        return res.status(404).send('not found');

    if (_.isBoolean(body.completed) && body.completed) {
        body.completedAt = Date.now();
    }
    else {
        body.completedAt = null;
        body.completed = false;
    }

    Todo.findOneAndUpdate({ _id: id, _creator: req.user._id }, { $set: body }, { new: true })
        .then(todo => {
            if (!todo)
                return res.status(404).send('not found');
            res.send({ todo });
        })
        .catch(err => res.status(400).send(err));

});

// POST /users
app.post('/users', (req, res) => {
    const user = new User(_.pick(req.body, ['email', 'password']));
    user.save()
        .then(user => user.generateAuthToken())
        .then(token => {
            res.status(201).header('x-auth', token).send(user);
        })
        .catch(err => res.status(400).send(err));
});


// GET /users/me
app.get('/users/me', authenticate, (req, res) => {
    res.send(req.user);
});

// POST: /users/login
app.post('/users/login', (req, res) => {
    const body = _.pick(req.body, ['email', 'password']);
    User.findByCredencials(body.email, body.password)
        .then(user => {
            if (!user) res.status(400).send();
            user.generateAuthToken().then(token => {
                res.header('x-auth', token).send(user);
            });
        })
        .catch(err => res.status(400).send(err));
});

//DELETE /users/me/token
app.delete('/users/me/token', authenticate, (req, res) => {
    req.user.removeToken(req.token)
        .then(() => res.send())
        .catch(() => res.status.send());
});

app.listen(port, () => {
    console.log(`Server Started at ${port}`);
});

module.exports = {
    app
}