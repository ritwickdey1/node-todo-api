const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');

const { app } = require('./../server');
const { Todo } = require('./../models/todo');
const { User } = require('./../models/user');
const { fakeTodos, populateTodos, fakeUsers, populateUsers } = require('./seed/seed');

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
    it('should create a new todo', (done) => {
        let text = `Test todo text`;
        const xAuth = fakeUsers[0].tokens[0].token;

        request(app)
            .post('/todos')
            .set('x-auth', xAuth)
            .send({ text })
            .expect(201)
            .expect((res) => {
                expect(res.body.text).toBe(text);
            })
            .end((err, res) => {
                if (err)
                    return done(err);

                Todo.find({ text })
                    .then(todos => {
                        expect(todos.length).toBe(1);
                        expect(todos[0].text).toBe(text);
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it('should not create data with invalid data', done => {
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .post('/todos')
            .set('x-auth', xAuth)
            .send({})
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);

                Todo.find()
                    .then(todos => {
                        expect(todos.length).toBe(fakeTodos.length);
                        done();
                    })
                    .catch(err => done(err));
            })
    });

});

describe('GET /todos', () => {
    const xAuth = fakeUsers[0].tokens[0].token;
    it('should get all todos', (done) => {
        request(app)
            .get('/todos')
            .set('x-auth', xAuth)
            .expect(200)
            .expect(res => {
                expect(res.body.todos.length).toBe(2);
            })
            .end(done);
    });

});

describe('GET /todos/:id', () => {

    it('should return currect todo doc', (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        const fakeTodo = fakeTodos[0];
        request(app)
            .get(`/todos/${fakeTodo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .expect(200)
            .expect(res => {
                expect(res.body.todo.text).toBe(fakeTodo.text)
            })
            .end(done);
    });

    it('should NOT return todo doc created by others', (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        const fakeTodo = fakeTodos[1];
        request(app)
            .get(`/todos/${fakeTodo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .expect(404)
            .expect(res => {
                expect(res.body.todo).toNotExist();
            })
            .end(done);
    });

    it('should return 404 if todo not found', (done) => {
        const fakeTodoId = new ObjectID();
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .get(`/todos/${fakeTodoId.toHexString()}`)
            .set('x-auth', xAuth)
            .expect(404)
            .end(done);
    });

    it('should return 404 for fake todo id', (done) => {
        const fakeTodoId = 125;
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .get(`/todos/${fakeTodoId}`)
            .set('x-auth', xAuth)
            .expect(404)
            .end(done);
    });

});

describe('DELETE /todos/:id', () => {

    it('should delete todo perfectly', (done) => {
        const todo = fakeTodos[0];
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .delete(`/todos/${todo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .expect(200)
            .expect(res => {
                expect(res.body.todo.text).toBe(todo.text);
            })
            .end((err, res) => {
                if (err)
                    return done(err);
                Todo.findById(todo._id)
                    .then(res => {
                        expect(res).toEqual(null);
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it('should NOT delete todo created by others', (done) => {
        const todo = fakeTodos[1];
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .delete(`/todos/${todo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .expect(404)
            .expect(res => {
                expect(res.body.todo).toNotExist();
            })
            .end((err, res) => {
                if (err)
                    return done(err);
                Todo.findById(todo._id)
                    .then(res => {
                        expect(res).toExist()
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it('should return 404 if Id not exists', (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .delete(`/todos/${new ObjectID().toHexString()}`)
            .set('x-auth', xAuth)
            .expect(404)
            .end(done);
    });

    it('should return 404 if invalid id', (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        request(app)
            .delete(`/todos/125`)
            .set('x-auth', xAuth)
            .expect(404)
            .end(done);

    });
});

describe('PATCH /todos/:id', () => {
    it("should update todo", (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        const todo = fakeTodos[0];
        const newText = 'my custom text';
        request(app)
            .patch(`/todos/${todo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .send({
                text: newText,
                completed: true
            })
            .expect(200)
            .expect(res => {
                expect(res.body.todo.text).toBe(newText);
                expect(res.body.todo.completed).toBe(true);
                expect(res.body.todo.completedAt).toBeLessThan(Date.now());

            })
            .end((err, res) => {
                if (err)
                    return done(err);
                Todo.findById(todo._id)
                    .then(res => {
                        expect(res.text).toBe(newText);
                        expect(res.completed).toBe(true);
                        expect(res.completedAt).toBeLessThan(Date.now());
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it("should NOT update todo created by others", (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        const todo = fakeTodos[1];
        const newText = 'my custom text';
        request(app)
            .patch(`/todos/${todo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .send({
                text: newText,
                completed: true
            })
            .expect(404)
            .expect(res => {
                expect(res.body.todo).toNotExist()
            })
            .end((err, res) => {
                if (err)
                    return done(err);
                Todo.findById(todo._id)
                    .then(res => {
                        expect(res.text).toNotBe(newText);
                        expect(res.text).toBe(todo.text);
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it("should should clear `completedAt` when todo is not completed", (done) => {
        const xAuth = fakeUsers[0].tokens[0].token;
        const todo = fakeTodos[0];
        const newText = 'my custom text';
        request(app)
            .patch(`/todos/${todo._id.toHexString()}`)
            .set('x-auth', xAuth)
            .send({
                text: newText
            })
            .expect(200)
            .expect(res => {
                expect(res.body.todo.text).toBe(newText);
                expect(res.body.todo.completed).toBe(false);
                expect(res.body.todo.completedAt).toBe(null);

            })
            .end((err, res) => {
                if (err)
                    return done(err);
                Todo.findById(todo._id)
                    .then(res => {
                        expect(res.text).toBe(newText);
                        expect(res.completed).toBe(false);
                        expect(res.completedAt).toBe(null);
                        done();
                    })
                    .catch(e => done(e));
            });

    });
});

describe('GET /user/me', () => {
    it('should return user if athenticated', (done) => {
        request(app)
            .get('/users/me')
            .set('x-auth', fakeUsers[0].tokens[0].token)
            .expect(200)
            .expect(res => {
                expect(res.body._id).toBe(fakeUsers[0]._id.toHexString());
                expect(res.body.email).toBe(fakeUsers[0].email);
            })
            .end(done);
    });

    it('should return 401 if NOT athenticated', (done) => {
        request(app)
            .get('/users/me')
            .expect(401)
            .expect(res => {
                expect(res.body).toEqual({});
            })
            .end(done);

    });
});

describe('POST /users', () => {
    it('should create a user', (done) => {
        const email = 'hello@ritwickdey.io';
        const password = 'hello@1455!@';

        request(app)
            .post('/users')
            .send({ email, password })
            .expect(201)
            .expect(res => {
                expect(res.headers['x-auth']).toExist();
                expect(res.body._id).toExist();
                expect(res.body.email).toEqual(email);
            })
            .end(err => {
                err && done(err); // :D

                User.findOne({ email })
                    .then(user => {
                        expect(user).toExist();
                        expect(user.password).toNotBe(password);
                        done();
                    })
                    .catch(e => done(e));
            });
    });

    it('should validate errors if request is invalid', (done) => {

        const email = 'error';
        const password = 'hello@1455!@';

        request(app)
            .post('/users')
            .send({ email, password })
            .expect(400)
            .end(done);
    });

    it('should NOT create user if email in use', (done) => {
        const email = fakeUsers[0].email;
        const password = 'hello@1455!@';

        request(app)
            .post('/users')
            .send({ email, password })
            .expect(400)
            .end(done);
    });
});

describe('POST /users/login', () => {

    it('should login user and return auth token', (done) => {
        const fakeUser = fakeUsers[1];
        const email = fakeUser.email;
        const password = fakeUser.password;

        request(app)
            .post('/users/login')
            .send({ email, password })
            .expect(200)
            .expect(res => {
                expect(res.headers['x-auth']).toExist();
            })
            .end((err, res) => {
                err && done(err);
                const xAuth = res.headers['x-auth'];

                User.findById(fakeUser._id)
                    .then(user => {
                        expect(user.tokens[1]).toInclude({
                            token: xAuth
                        });
                        done();
                    })
                    .catch(err => done(err));
            });
    });


    it('should reject invalid login', (done) => {
        const email = 'falseEmail@example.com';
        const password = 'noPass';
        request(app)
            .post('/users/login')
            .send({ email, password })
            .expect(400)
            .expect(res => {
                expect(res.headers['x-auth']).toNotExist();
            })
            .end(done);
    });

});

describe('DELETE /users/me/token', (done) => {
    it('should remove auth token on logout', (done) => {
        const fakeUser = fakeUsers[0];
        const xAuth = fakeUser.tokens[0].token;

        request(app)
            .delete('/users/me/token')
            .set('x-auth', xAuth)
            .expect(200)
            .expect(res => {
                expect(res.headers['x-auth']).toNotExist();
            })
            .end((err, res) => {
                err && done(err);
                User.findById(fakeUser._id)
                    .then(user => {
                        expect(user.tokens.length).toEqual(0);
                        done();
                    })
                    .catch(e => done(e));
            });
    });
});
