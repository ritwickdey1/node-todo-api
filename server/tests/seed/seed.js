const { ObjectID } = require('mongodb');
const jwt = require('jsonwebtoken');
const { Todo } = require('../../models/todo');
const { User } = require('../../models/user');


const userOneId = new ObjectID();
const userTwoId = new ObjectID();
const fakeUsers = [
    {
        _id: userOneId,
        email: 'info@ritwickdey.com',
        password: 'pass123@!',
        tokens: [{
            access: 'auth',
            token: jwt.sign({
                _id: userOneId.toHexString(),
                access: 'auth'
            }, process.env.JWT_SECRET).toString()
        }]
    },
    {
        _id: userTwoId,
        email: 'my@example.com',
        password: 'myLongPass123@!',
        tokens: [{
            access: 'auth',
            token: jwt.sign({
                _id: userTwoId.toHexString(),
                access: 'auth'
            }, process.env.JWT_SECRET).toString()
        }]
    }
];

const fakeTodos = [
    {
        _id: new ObjectID(),
        text: "fake todo 1",
        _creator : userOneId
    },
    {
        _id: new ObjectID(),
        text: "fake todo 2",
        _creator : userTwoId
    },
    {
        _id: new ObjectID(),
        text: "fake todo 3",
        _creator : userOneId
    }
];


const populateTodos = (done) => {
    Todo.remove({}).then(() => {
        Todo.insertMany(fakeTodos)
            .then(() => done())
            .catch(err => done(err));
    });
};


const populateUsers = (done) => {
    User.remove({})
        .then(() => {
            let userOne = new User(fakeUsers[0]).save();
            let userTwo = new User(fakeUsers[1]).save();
            return Promise.all([userOne, userTwo]);
        })
        .then(() => done());
};

module.exports = {
    fakeTodos,
    populateTodos,

    fakeUsers,
    populateUsers
}